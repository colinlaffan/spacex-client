# SpaceX-Client

A demo application that consumes a [SpaceX REST API](https://github.com/r-spacex/SpaceX-API) to display launch data and provide the ability to sort/filter this data.

## Installation

To run this example project, clone the repo, and run `pod install` from the root directory first.

## Architecture

The application uses the following architectural design patterns throughout the project:
- MVVM: The main design pattern used throughout the project
- Coordinator: Used to handle navigation within the app. A good separation of concern pattern since it means ViewControllers do not reference other ViewControllers.
- Dependency Injection: A good design pattern for providing an object with the objects it needs to function.  Used in conjunction with the Protocol design pattern it makes it easier to mock data for Unit Tests.
- Repository: Repositories are classes that provide access to various data sources within the app (in this case the Network API Service).

## Unit Tests
The app showcases the AAA (Arrange-Act-Assert) pattern when it comes to unit testing, which ensures tests are structured in an easy to read manner and easier to maintain.

## Pods
The app uses the following CocoaPods:
- AlamofireImage: Used to download and cache images that are displayed on the main screen
