//
//  AppDelegate.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var appCoordinator: AppCoordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Initialize Services
        let networkServiceConfiguration = NetworkServiceConfiguration(baseUrl: "https://api.spacexdata.com/v4/", timeout: 30)
        let networkService = NetworkService(with: networkServiceConfiguration)
        
        // Initialize Repositories
        let companyRepository = CompanyRepository(with: networkService)
        let launchesRepository = LaunchesRepository(with: networkService)
        
        let services = Services(with: networkService,
                                companyRepository: companyRepository,
                                launchesRepository: launchesRepository)
        let window = UIWindow(frame: UIScreen.main.bounds)
        let appCoordinator = AppCoordinator(with: window, services: services)
        
        self.window = window
        self.appCoordinator = appCoordinator
        
        self.appCoordinator.start()
        
        return true
    }


}

