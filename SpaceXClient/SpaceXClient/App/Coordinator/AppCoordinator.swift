//
//  AppCoordinator.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import UIKit

class AppCoordinator: Coordinator {
    
    // MARK: -
    // MARK: INJECTED PROPERTIES

    let window: UIWindow
    let services: Services
    
    // MARK: -
    // MARK: PROPERTIES
    
    var rootViewController: UINavigationController?
    
    // MARK: -
    // MARK: INIT

    init(with window: UIWindow, services: Services) {
        self.window = window
        self.services = services

        // Set the launch screen to be the rootViewController
        let launchScreenStoryBoard = UIStoryboard(name: "LaunchScreen", bundle: nil)
        let viewController = launchScreenStoryBoard.instantiateInitialViewController()

        self.window.rootViewController = viewController
        self.window.makeKeyAndVisible()
    }
    
    // MARK: -
    // MARK: PROTOCOL IMPLEMENTATION
    
    var childCoordinators = [Coordinator]()
    
    func start() {
        self.loadCompanyView()
    }
    
}

extension AppCoordinator {
    
    // MARK: -
    // MARK: PRIVATE FUNCTIONS
    
    private func loadCompanyView() {
        let navigationController = UINavigationController(navigationBarClass: nil, toolbarClass: nil)
        let companyCoordinator = CompanyCoordinator(with: navigationController, and: self.services)
        
        companyCoordinator.start()

        self.window.rootViewController = navigationController
        self.window.makeKeyAndVisible()
        
        self.add(coordinator: companyCoordinator)
        self.rootViewController = navigationController
    }
    
}
