//
//  Coordinator.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import Foundation

protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] { get set }

    func start()
}

extension Coordinator {
    func add(coordinator: Coordinator) {
        childCoordinators.append(coordinator)
    }

    func remove<T>(_ type: T.Type) {
        childCoordinators = childCoordinators.filter { !($0 is T) }
    }
    
    func getChildCoordinator<T>(_ type: T.Type) -> Coordinator? {
        return childCoordinators.first(where: { ($0 is T) })
    }
}
