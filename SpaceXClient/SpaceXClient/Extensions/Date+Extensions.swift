//
//  Date+Extensions.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 10/02/2022.
//

import Foundation

extension Date {
    
    var toDateOnlyDisplayString: String {
        let dateFormatter = DateFormatter()

        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd/MM/yyyy"

        return dateFormatter.string(from: self)
    }
    var toTimeOnlyDisplayString: String {
        let dateFormatter = DateFormatter()

        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "HH:mm"

        return dateFormatter.string(from: self)
    }
    var toYearOnlyDisplayString: String {
        let dateFormatter = DateFormatter()

        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy"

        return dateFormatter.string(from: self)
    }
    
    static func from(isoAPIValue: String) -> Date? {
        let dateFormatter = DateFormatter()

        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")

        return dateFormatter.date(from: isoAPIValue)
    }
    
}
