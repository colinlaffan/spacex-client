//
//  UITableView+Extensions.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 09/02/2022.
//

import UIKit

extension UITableView {
    
    func registerNib<T: UITableViewCell>(of type: T.Type) {
        let nibName = String(describing: T.self)
        let nib = UINib(nibName: nibName, bundle: Bundle(for: T.self))
        
        self.register(nib, forCellReuseIdentifier: "\(T.self)")
    }
    
    func dequeueReusableCell<T: UITableViewCell>(of type: T.Type, for indexPath: IndexPath) -> T? {
        let cell = self.dequeueReusableCell(withIdentifier: "\(T.self)", for: indexPath) as? T
        
        return cell
    }
    
}
