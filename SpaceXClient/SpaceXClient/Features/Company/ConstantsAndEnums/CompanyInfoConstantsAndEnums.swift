//
//  CompanyInfoConstantsAndEnums.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import Foundation

struct CompanyInfoConstantsAndEnums {
    
    struct CompanyInfoView {
        
        enum Constants: String {
            case NavigationTitle = "SpaceX"
            case HeadingCompany = "COMPANY"
            case CompanyInfoText = "%@ was founded by %@ in %@. It has now %@ employees, %@ launch sites, and is valued at USD %@"
            case HeadingLaunches = "LAUNCHES"
            case CellLabelMission = "Mission:"
            case CellLabelDateTime = "Date/time:"
            case CellLabelRocket = "Rocket:"
            case CellLabelDaysSince = "Days since now:"
            case CellLabelDaysFrom = "Days from now:"
            case NoContentErrorTitle = "Error Loading Data"
            case NoContentErrorSubTitle = "There was a problem loading the data. Please try again later."
            case AlertMessage = "How would you like to open the article?"
            case WikipediaActionTitle = "Wikipedia"
            case VideoPagesActionTitle = "Video Pages"
            case CancelActionTitle = "Cancel"
        }
        
    }
    
    struct SortFilterView {
        
        enum Constants: String {
            case NavigationTitle = "Sort/Filter"
        }
        
    }
    
}
