//
//  CompanyCoordinator.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import UIKit

class CompanyCoordinator: Coordinator {
    
    // MARK: -
    // MARK: INJECTED PROPERTIES
    
    let presenter: UINavigationController
    let services: Services
    
    // MARK: -
    // MARK: INIT

    init(with presenter: UINavigationController, and services: Services) {
        self.presenter = presenter
        self.services = services
    }
    
    // MARK: -
    // MARK: PROTOCOL IMPLEMENTATION
    
    var childCoordinators = [Coordinator]()
    
    func start() {
        let viewModel = CompanyInfoViewModel(with: self.services.companyRepository,
                                             launchesRepository: self.services.launchesRepository)
        let viewController = CompanyInfoViewController(with: viewModel)
        
        viewController.delegate = self
        
        self.presenter.pushViewController(viewController, animated: true)
    }
    
}

extension CompanyCoordinator: CompanyInfoViewControllerDelegate {
    
    func companyInfoViewControllerOpenFiltersView(_ viewController: CompanyInfoViewController) {
        let viewModel = SortFilterViewModel(with: self.services.launchesRepository)
        let viewController = SortFilterViewController(with: viewModel)
        let navigationController = UINavigationController(rootViewController: viewController)
        
        viewController.delegate = self
        navigationController.modalPresentationStyle = .fullScreen
        navigationController.navigationBar.tintColor = .label
        
        self.presenter.present(navigationController, animated: true, completion: nil)
    }
    
}

extension CompanyCoordinator: SortFilterViewControllerDelegate {
    
    func sortFilterViewControllerShouldClose(_ viewController: SortFilterViewController) {
        self.presenter.dismiss(animated: true, completion: nil)
    }
    
}
