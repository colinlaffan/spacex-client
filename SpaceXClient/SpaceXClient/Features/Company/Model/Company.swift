//
//  Company.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import Foundation

struct Company {
    
    let name: String
    let founderName: String
    let yearFounded: Int
    let numberOfEmployees: Int
    let numberOfLaunchSites: Int
    let valuation: Int
    
    init(with networkDTO: CompanyInfoDTO) {
        self.name = networkDTO.name
        self.founderName = networkDTO.founder
        self.yearFounded = networkDTO.founded
        self.numberOfEmployees = networkDTO.employees
        self.numberOfLaunchSites = networkDTO.launchSites
        self.valuation = networkDTO.valuation
    }
    
}
