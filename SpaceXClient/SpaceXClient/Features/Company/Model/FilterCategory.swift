//
//  FilterCategory.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 13/02/2022.
//

import Foundation

struct FilterCategory {
    
    let option: LaunchesRepositoryEnums.Filters.Options
    var filters: [String]
    
}
