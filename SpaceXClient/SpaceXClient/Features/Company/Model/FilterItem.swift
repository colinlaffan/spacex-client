//
//  FilterItem.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 13/02/2022.
//

import Foundation

struct FilterItem {
    
    let name: String
    var isSelected: Bool
    
}
