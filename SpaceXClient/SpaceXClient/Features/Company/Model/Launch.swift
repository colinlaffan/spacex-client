//
//  Launch.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 09/02/2022.
//

import Foundation

class Launch {
    
    let missionName: String
    let launchDate: Date?
    let wasSuccessful: Bool
    let rocket: Rocket?
    let smallPatchImageUrlPath: String?
    
    var rocketName: String {
        return self.rocket?.name ?? ""
    }
    var rocketType: String {
        return self.rocket?.type ?? ""
    }
    var numberOfDaysSinceLaunch: Int? {
        guard let launchDate = self.launchDate else { return nil }
        let calendar = Calendar.current
        let numberOfDays = calendar.dateComponents([.day], from: Date(), to: launchDate)
        
        return numberOfDays.day
    }
    
    init(with networkDTO: LaunchDTO) {
        self.missionName = networkDTO.name
        self.launchDate = networkDTO.dateUTC
        self.wasSuccessful = networkDTO.success ?? false
        self.smallPatchImageUrlPath = networkDTO.links?.patch?.small
        
        if let rocket = networkDTO.rocket {
            self.rocket = Rocket(with: rocket)
        } else {
            self.rocket = nil
        }
    }
    
}
