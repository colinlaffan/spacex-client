//
//  Rocket.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 10/02/2022.
//

import Foundation

struct Rocket {
    
    let id: String
    let name: String
    let type: String
    
    init(with networkDTO: RocketDTO) {
        self.id = networkDTO.id
        self.name = networkDTO.name
        self.type = networkDTO.type
    }
    
}
