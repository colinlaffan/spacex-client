//
//  CompanyInfoViewController.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import UIKit

protocol CompanyInfoViewControllerDelegate: AnyObject {
  func companyInfoViewControllerOpenFiltersView(_ viewController: CompanyInfoViewController)
}

class CompanyInfoViewController: UIViewController {

    // MARK: -
    // MARK: VIEW OUTLETS
    
    @IBOutlet weak var noContentContainerView: UIView!
    @IBOutlet weak var noContentImageView: UIImageView!
    @IBOutlet weak var noContentTitleLabel: UILabel!
    @IBOutlet weak var noContentSubTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: -
    // MARK: INJECTED DEPENDENCIES

    var viewModel: CompanyInfoViewModel!
    
    // MARK: -
    // MARK: DELEGATES
    
    weak var delegate: CompanyInfoViewControllerDelegate?
    
    // MARK: -
    // MARK: INITIALIZATION

    init(with viewModel: CompanyInfoViewModel) {
        self.viewModel = viewModel

        super.init(nibName: "CompanyInfoViewController", bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: -
    // MARK: VIEW LIFECYCLE

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        self.bindViewModel()
        self.viewModel.load()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.viewDidAppear()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.viewWillAppear()
    }
    
    // MARK: -
    // MARK: UI SETUP FUNCTIONS
    
    func setupUI() {
        self.setupNavigationBar()
        self.setupTableView()
    }
    
    func setupNavigationBar() {
        let barButtonItem = UIBarButtonItem(image: UIImage(systemName: "line.3.horizontal.decrease.circle"), style: .plain, target: self, action: #selector(didTapFilterButton))
        
        barButtonItem.tintColor = .label
        
        self.title = self.viewModel.navigationTitle
        self.navigationItem.rightBarButtonItem = barButtonItem
    }
    
    func setupTableView() {
        self.tableView.registerNib(of: HeadingOneLineTableViewCell.self)
        self.tableView.registerNib(of: ListSummaryTableViewCell.self)
        self.tableView.registerNib(of: DetailsCardTableViewCell.self)
    }
    
    // MARK: -
    // MARK: VIEW ACTIONS
    
    @objc func didTapFilterButton() {
        self.delegate?.companyInfoViewControllerOpenFiltersView(self)
    }
    
    // MARK: -
    // MARK: VIEW MODEL FUNCTIONS
    
    func bindViewModel() {
        self.viewModel.isTableViewHidden.bindAndFire { [weak self] isTableViewHidden in
            DispatchQueue.main.async {
                self?.tableView.isHidden = isTableViewHidden
            }
        }
        self.viewModel.isNoContentContainerViewHidden.bindAndFire { [weak self] isNoContentContainerViewHidden in
            DispatchQueue.main.async {
                self?.noContentContainerView.isHidden = isNoContentContainerViewHidden
            }
        }
        self.viewModel.noContentTitleText.bindAndFire { [weak self] noContentTitleText in
            DispatchQueue.main.async {
                self?.noContentTitleLabel.text = noContentTitleText
            }
        }
        self.viewModel.noContentSubTitleText.bindAndFire { [weak self] noContentSubTitleText in
            DispatchQueue.main.async {
                self?.noContentSubTitleLabel.text = noContentSubTitleText
            }
        }
        self.viewModel.showLoadingActivity = { [weak self] in
            DispatchQueue.main.async {
                guard let strongSelf = self else { return }
                
                strongSelf.activityIndicator.startAnimating()
                strongSelf.activityIndicator.isHidden = false
            }
        }
        self.viewModel.hideLoadingActivity = { [weak self] in
            DispatchQueue.main.async {
                guard let strongSelf = self else { return }
                
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.activityIndicator.isHidden = true
            }
        }
        self.viewModel.loadTableViewData = { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        self.viewModel.displayOpenOptionsAlert = { [weak self] title in
            self?.displayOpenOptionsAlert(with: title)
        }
    }
    
    func displayOpenOptionsAlert(with title: String) {
        let message = CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.AlertMessage.rawValue
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        let wikipediaActionTitle = CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.WikipediaActionTitle.rawValue
        let videoPagesActionTitle = CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.VideoPagesActionTitle.rawValue
        let cancelActionTitle = CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.CancelActionTitle.rawValue
        let wikipediaAction = UIAlertAction(title: wikipediaActionTitle, style: .default, handler: nil)
        let videoPagesAction = UIAlertAction(title: videoPagesActionTitle, style: .default, handler: nil)
        let cancelAction = UIAlertAction(title: cancelActionTitle, style: .cancel, handler: nil)
        
        alertController.addAction(wikipediaAction)
        alertController.addAction(videoPagesAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true)
    }
    
}

extension CompanyInfoViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: -
    // MARK: TABLEVIEW DELEGATE & DATASOURCE FUNCTIONS

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellViewModel = self.viewModel.getCellViewModel(at: indexPath.row)
        var cell = UITableViewCell()
        
        if cellViewModel.cellType == .HeadingOneLabel {
            cell = HeadingOneLineTableViewCell.with(viewModel: cellViewModel, tableView: tableView, indexPath: indexPath)
        } else if cellViewModel.cellType == .ListSummary {
            cell = ListSummaryTableViewCell.with(viewModel: cellViewModel, tableView: tableView, indexPath: indexPath)
        } else if cellViewModel.cellType == .DetailsCard {
            cell = DetailsCardTableViewCell.with(viewModel: cellViewModel, tableView: tableView, indexPath: indexPath)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.viewModel.didSelectCell(at: indexPath.row)
    }
    
}
