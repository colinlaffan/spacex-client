//
//  SortFilterViewController.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 12/02/2022.
//

import UIKit

protocol SortFilterViewControllerDelegate: AnyObject {
  func sortFilterViewControllerShouldClose(_ viewController: SortFilterViewController)
}

class SortFilterViewController: UIViewController {

    // MARK: -
    // MARK: VIEW OUTLETS
    
    @IBOutlet weak var sortOrderSegmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!

    // MARK: -
    // MARK: INJECTED DEPENDENCIES

    var viewModel: SortFilterViewModel!
    
    // MARK: -
    // MARK: DELEGATES
    
    weak var delegate: SortFilterViewControllerDelegate?
    
    // MARK: -
    // MARK: INITIALIZATION

    init(with viewModel: SortFilterViewModel) {
        self.viewModel = viewModel

        super.init(nibName: "SortFilterViewController", bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: -
    // MARK: VIEW LIFECYCLE

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        self.viewModel.load()
    }
    
    // MARK: -
    // MARK: VIEW ACTIONS
    
    @IBAction func sortOrderSegmentControlValueDidChange(_ segmentControl: UISegmentedControl) {
        self.viewModel.didSelectSortOrderSegment(at: segmentControl.selectedSegmentIndex)
    }
    
    @IBAction func didTapSaveBarButton() {
        self.viewModel.didTapSaveButton()
        self.delegate?.sortFilterViewControllerShouldClose(self)
    }
    
    // MARK: -
    // MARK: UI SETUP FUNCTIONS
    
    func setupUI() {
        self.setupNavigationBar()
        self.setupSegmentedControl()
        self.setupTableView()
    }
    
    func setupNavigationBar() {
        self.title = self.viewModel.navigationTitle
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(self.didTapSaveBarButton))
    }
    
    func setupSegmentedControl() {
        self.sortOrderSegmentedControl.selectedSegmentIndex = self.viewModel.defaultSortOrderIndex
    }
    
    func setupTableView() {
        self.tableView.registerNib(of: SelectableTableViewCell.self)
    }
    
}

extension SortFilterViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: -
    // MARK: TABLEVIEW DELEGATE & DATASOURCE FUNCTIONS

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.numberOfSections
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.viewModel.getSectionTitle(at: section)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.getNumberOfCellsForSection(at: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellViewModel = self.viewModel.getCellViewModel(at: indexPath.row, in: indexPath.section)
        let cell = SelectableTableViewCell.with(viewModel: cellViewModel, tableView: tableView, indexPath: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.viewModel.didSelectCell(at: indexPath.row, in: indexPath.section)
    }
    
}
