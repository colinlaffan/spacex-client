//
//  CompanyInfoViewModel.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import Foundation

class CompanyInfoViewModel {
    
    // MARK: -
    // MARK: INJECTED PROPERTIES
    
    let companyRepository: CompanyRepositoryProtocol
    let launchesRepository: LaunchesRepositoryProtocol
    
    // MARK: -
    // MARK: COMPUTED PROPERTIES
    
    var navigationTitle: String {
        return CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.NavigationTitle.rawValue
    }
    var companyInfoText: String {
        let companyInfoRawTex = CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.CompanyInfoText.rawValue
        
        guard let company = self.companyRepository.getCompany() else { return companyInfoRawTex }
        
        let companyName = company.name
        let founderName = company.founderName
        let year = "\(company.yearFounded)"
        let employees = "\(company.numberOfEmployees)"
        let launchSites = "\(company.numberOfLaunchSites)"
        let valuation = "\(company.valuation)"
        
        return String(format: companyInfoRawTex, arguments: [companyName, founderName, year, employees, launchSites, valuation])
    }
    var numberOfCells: Int {
      return self.cellViewModels.count
    }
    
    // MARK: -
    // MARK: PROPERTIES
    
    var isLoading: Bool = false
    var cellViewModels = [CellViewModel]() {
        didSet {
            self.loadTableViewData?()
        }
    }
    
    // MARK: -
    // MARK: BINDED PROPERTIES

    let isTableViewHidden: DynamicProperty<Bool> = DynamicProperty(false)
    let isNoContentContainerViewHidden: DynamicProperty<Bool> = DynamicProperty(true)
    let noContentTitleText: DynamicProperty<String> = DynamicProperty("")
    let noContentSubTitleText: DynamicProperty<String> = DynamicProperty("")
    
    // MARK: -
    // MARK: BINDED CLOSURES
    
    var loadTableViewData: (() -> Void)?
    var showLoadingActivity: (() -> Void)?
    var hideLoadingActivity: (() -> Void)?
    var displayOpenOptionsAlert: ((String) -> Void)?
    
    // MARK: -
    // MARK: INITIALIZATION
    
    required init(with companyRepository: CompanyRepositoryProtocol,
                  launchesRepository: LaunchesRepositoryProtocol) {
        self.companyRepository = companyRepository
        self.launchesRepository = launchesRepository
    }
    
    // MARK: -
    // MARK: VIEW MODEL FUNCTIONS
    
    func viewWillAppear() {
        if !self.isLoading {
            self.showLoadingActivity?()
            self.isTableViewHidden.value = true
            self.loadLaunches()
        }
    }
    
    func viewDidAppear() {
        self.isLoading = false
    }
    
    func load() {
        self.isLoading = true
        self.showLoadingActivity?()
        self.loadCompanyInfo()
    }
    
    func loadCompanyInfo() {
        self.companyRepository.fetchAllCompanyInfo { [weak self] success in
            if success {
                self?.loadLaunches()
            } else {
                self?.handleLoadingError()
            }
        }
    }
    
    func loadLaunches() {
        self.launchesRepository.fetchAllLaunches { [weak self] success in
            if success {
                self?.handleSuccessfulLoad()
            } else {
                self?.handleLoadingError()
            }
        }
    }
    
    func handleSuccessfulLoad() {
        self.hideLoadingActivity?()
        self.populateCellViewModels()
        self.isNoContentContainerViewHidden.value = true
        self.isTableViewHidden.value = false
    }
    
    func handleLoadingError() {
        self.hideLoadingActivity?()
        self.noContentTitleText.value = CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.NoContentErrorTitle.rawValue
        self.noContentSubTitleText.value = CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.NoContentErrorSubTitle.rawValue
        self.isTableViewHidden.value = true
        self.isNoContentContainerViewHidden.value = false
    }
    
    // MARK: -
    // MARK: CELL VIEW MODEL FUNCTIONS
    
    func getCellViewModel(at index: Int) -> CellViewModel {
        return self.cellViewModels[index]
    }
    
    func didSelectCell(at index: Int) {
        let cellViewModel = self.getCellViewModel(at: index)
        
        if cellViewModel.cellType == .DetailsCard, let viewModel = cellViewModel as? DetailsCardCellViewModel {
            self.displayOpenOptionsAlert?(viewModel.valueLabel1Text)
        }
    }
    
    func populateCellViewModels() {
        let companyHeaderViewModel = self.companyHeaderViewModel()
        let companyInfoViewModel = self.companyInfoViewModel()
        let launchesHeaderViewModel = self.launchesHeaderViewModel()
        let launchesViewModels = self.launchesViewModels()
        
        self.cellViewModels = [companyHeaderViewModel, companyInfoViewModel, launchesHeaderViewModel] + launchesViewModels
    }
    
    func companyHeaderViewModel() -> CellViewModel {
        var cellData = CellData(with: .HeadingOneLabel)
        
        cellData.titleText = CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.HeadingCompany.rawValue
        
        return HeadingOneLineCellViewModel(with: cellData)
    }
    
    func companyInfoViewModel() -> CellViewModel {
        var cellData = CellData(with: .ListSummary)
        
        cellData.titleText = self.companyInfoText
        
        return ListSummaryCellViewModel(with: cellData)
    }
    
    func launchesHeaderViewModel() -> CellViewModel {
        var cellData = CellData(with: .HeadingOneLabel)
        
        cellData.titleText = CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.HeadingLaunches.rawValue
        
        return HeadingOneLineCellViewModel(with: cellData)
    }
    
    func launchesViewModels() -> [CellViewModel] {
        let launches = self.launchesRepository.getLaunches() 
        let cellData = launches.map({ (launchModel: Launch) -> CellData in
            var cellData = CellData(with: .DetailsCard)
            
            cellData.imageUrlPath = launchModel.smallPatchImageUrlPath
            cellData.labelText1 = CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.CellLabelMission.rawValue
            cellData.detailText1 = launchModel.missionName
            cellData.labelText2 = CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.CellLabelDateTime.rawValue
            
            if let launchDate = launchModel.launchDate {
                cellData.detailText2 = launchDate.toDateOnlyDisplayString + " at " + launchDate.toTimeOnlyDisplayString
            }
            
            cellData.labelText3 = CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.CellLabelRocket.rawValue
            cellData.detailText3 = launchModel.rocketName + " / " + launchModel.rocketType
            
            if let numberOfDaysSinceLaunch = launchModel.numberOfDaysSinceLaunch {
                if numberOfDaysSinceLaunch > 0 {
                    cellData.labelText4 = CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.CellLabelDaysFrom.rawValue
                    cellData.detailText4 = "\(numberOfDaysSinceLaunch)"
                } else {
                    cellData.labelText4 = CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.CellLabelDaysSince.rawValue
                    cellData.detailText4 = "\(abs(numberOfDaysSinceLaunch))"
                }
            } else {
                cellData.labelText4 = CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.CellLabelDaysFrom.rawValue
            }
            
            cellData.detailBool1 = launchModel.wasSuccessful
            
            return cellData
        })
        
        return cellData.map { DetailsCardCellViewModel(with: $0) }
    }
    
}
