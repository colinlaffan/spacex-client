//
//  FilterCategorySectionCellViewModel.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 13/02/2022.
//

import Foundation

struct FilterCategorySectionCellViewModel {
    
    // MARK: -
    // MARK: INJECTED PROPERTIES
    
    let model: FilterCategory
    
    // MARK: -
    // MARK: COMPUTED PROPERTIES
    
    var filterOption: LaunchesRepositoryEnums.Filters.Options {
        return self.model.option
    }
    var title: String {
        return self.model.option.rawValue
    }
    var numberOfCells: Int {
      return self.cellViewModels.count
    }
    
    // MARK: -
    // MARK: PROPERTIES
    
    var cellViewModels = [CellViewModel]()
    
    // MARK: -
    // MARK: INITIALIZATION
    
    init(with model: FilterCategory) {
        self.model = model
        self.cellViewModels = model.filters.map { filter in
            var cellData = CellData(with: .Selectable)
            
            cellData.titleText = filter
            
            return SelectableCellViewModel(with: cellData)
        }
    }
    
    // MARK: -
    // MARK: CELL VIEW MODEL FUNCTIONS
    
    func getCellViewModel(at index: Int) -> CellViewModel {
        return self.cellViewModels[index]
    }
    
    func didSelectCell(at index: Int){
        guard let cellViewModel = self.cellViewModels[index] as? SelectableCellViewModel else { return }
        
        cellViewModel.didSelectCell()
    }
    
    func getSelectedCells() -> [CellViewModel]? {
        let selectedCells = self.cellViewModels.filter { cellViewModel in
            guard let cellViewModel = cellViewModel as? SelectableCellViewModel else { return false }
            
            return cellViewModel.isSelected.value
        }
        
        return selectedCells.isEmpty ? nil : selectedCells
    }
    
    func getSelectedCellsText() -> [String] {
        return self.cellViewModels.compactMap({(cellViewModel: CellViewModel) -> String? in
            guard let cellViewModel = cellViewModel as? SelectableCellViewModel else { return nil }
            
            return cellViewModel.isSelected.value ? cellViewModel.titleText : nil
        })
    }
    
}
