//
//  FilterCellViewModel.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 13/02/2022.
//

import Foundation

struct FilterCellViewModel {
    
    let model: String
    
    var title: String {
        return model
    }
    
}
