//
//  SortFilterViewModel.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 12/02/2022.
//

import Foundation

class SortFilterViewModel {
    
    // MARK: -
    // MARK: INJECTED PROPERTIES
    
    let launchesRepository: LaunchesRepositoryProtocol
    
    // MARK: -
    // MARK: COMPUTED PROPERTIES
    
    var navigationTitle: String {
        return CompanyInfoConstantsAndEnums.SortFilterView.Constants.NavigationTitle.rawValue
    }
    var defaultSortOrderIndex: Int {
        let rawSortOrder = self.launchesRepository.getRawSortOrder()
        
        if rawSortOrder == "desc" {
            return 1
        }
        
        return 0
    }
    var numberOfSections: Int {
      return self.sectionViewModels.count
    }
    
    // MARK: -
    // MARK: PROPERTIES
    
    var sectionViewModels = [FilterCategorySectionCellViewModel]()
    
    // MARK: -
    // MARK: INITIALIZATION
    
    init(with launchesRepository: LaunchesRepositoryProtocol) {
        self.launchesRepository = launchesRepository
    }
    
    // MARK: -
    // MARK: VIEW MODEL FUNCTIONS
    
    func load() {
        let filterData = self.launchesRepository.getFilterData()
        
        self.sectionViewModels = filterData.map { model in
            let sectionViewModel = FilterCategorySectionCellViewModel(with: model)
            
            return sectionViewModel
        }
    }
    
    func didTapSaveButton() {
        self.launchesRepository.clearFilterData()
        
        let sectionsWithSelectedCells = self.sectionViewModels.filter { sectionViewModel in
            return sectionViewModel.getSelectedCells() != nil
        }
        
        for sectionViewModel in sectionsWithSelectedCells {
            let selectedCellsText = sectionViewModel.getSelectedCellsText()
            
            if !selectedCellsText.isEmpty {
                self.launchesRepository.addFilter(sectionViewModel.filterOption, selectedValues: selectedCellsText)
            }
        }
    }
    
    func didSelectSortOrderSegment(at index: Int) {
        if index == 0 {
            self.launchesRepository.setSortOrder("asc")
        } else if index == 1 {
            self.launchesRepository.setSortOrder("desc")
        }
    }
    
    func getSectionTitle(at index: Int) -> String {
        let sectionViewModel = self.sectionViewModels[index]
        
        return sectionViewModel.title
    }
    
    func getNumberOfCellsForSection(at index: Int) -> Int {
        let sectionViewModel = self.sectionViewModels[index]
        
        return sectionViewModel.numberOfCells
    }
    
    func getCellViewModel(at index: Int, in section: Int) -> CellViewModel {
        let sectionViewModel = self.sectionViewModels[section]
        
        return sectionViewModel.getCellViewModel(at: index)
    }
    
    func didSelectCell(at index: Int, in section: Int) {
        let sectionViewModel = self.sectionViewModels[section]
        
        sectionViewModel.didSelectCell(at: index)
    }
    
}
