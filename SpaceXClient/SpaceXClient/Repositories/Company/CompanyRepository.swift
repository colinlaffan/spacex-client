//
//  CompanyRepository.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import Foundation

class CompanyRepository: CompanyRepositoryProtocol {
    
    // MARK: -
    // MARK: INJECTED PROPERTIES
    
    let networkService: NetworkServiceProtocol
    
    // MARK: -
    // MARK: PRIVATE PROPERTIES
    
    var company: Company?
    
    // MARK: -
    // MARK: PROTOCOL IMPLEMENTATION
    
    required init(with networkService: NetworkServiceProtocol) {
        self.networkService = networkService
    }
    
    func fetchAllCompanyInfo(callback: @escaping (Bool) -> Void) {
        let resource = GetAllCompanyInfoResource()

        self.networkService.load(using: resource) { response, error in
            if let response = response {
                self.company = Company(with: response)
                callback(true)
            } else {
                self.company = nil
                callback(false)
            }
        }
    }
    
    func getCompany() -> Company? {
        return self.company
    }
    
}
