//
//  CompanyRepositoryProtocol.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import Foundation

protocol CompanyRepositoryProtocol {
    
    init(with networkService: NetworkServiceProtocol)
    
    func fetchAllCompanyInfo(callback: @escaping (Bool) -> Void)
    func getCompany() -> Company?
    
}
