//
//  LaunchesRepositoryEnums.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 13/02/2022.
//

import Foundation

struct LaunchesRepositoryEnums {
    
    struct Filters {
        
        enum Options: String {
            case SuccessfulLaunch = "SUCCESSFUL LAUNCH"
            case LaunchYear = "LAUNCH YEAR"
            
            static func from(apiFilterOption: NetworkServiceConstantsEnums.FilterOptions) -> Options? {
                switch apiFilterOption {
                case .Success:
                    return .SuccessfulLaunch
                case .DateUTC:
                    return .LaunchYear
                }
            }
        }
        
    }
    
}
