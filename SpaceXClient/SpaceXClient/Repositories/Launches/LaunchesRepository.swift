//
//  LaunchesRepository.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import Foundation

class LaunchesRepository: LaunchesRepositoryProtocol {
    
    // MARK: -
    // MARK: INJECTED PROPERTIES
    
    let networkService: NetworkServiceProtocol
    
    // MARK: -
    // MARK: PRIVATE PROPERTIES
    
    var launches = [Launch]()
    var sortOrder = NetworkServiceConstantsEnums.SortOrder.Descending
    var filters = [FilterCategory]()
    var filterLaunchYears = [String]()
    
    // MARK: -
    // MARK: PROTOCOL IMPLEMENTATION
    
    required init(with networkService: NetworkServiceProtocol) {
        self.networkService = networkService
    }
    
    func fetchAllLaunches(callback: @escaping (Bool) -> Void) {
        let queryOptions = self.mapFiltersToQueryOptions()
        let resource = PostAllLaunchesResource(with: self.sortOrder, and: queryOptions)

        self.networkService.load(using: resource) { response, error in
            if let response = response?.launhes {
                self.launches = response.map { Launch(with: $0) }
                callback(true)
            } else {
                self.launches.removeAll()
                callback(false)
            }
        }
    }
    
    func getLaunches() -> [Launch] {
        return self.launches
    }
    
    func getRawSortOrder() -> String {
        return self.sortOrder.rawValue
    }
    
    func setSortOrder(_ selectedSort: String) {
        guard let newSortOrder = NetworkServiceConstantsEnums.SortOrder(rawValue: selectedSort) else { return }
        
        self.sortOrder = newSortOrder
    }
    
    func clearFilterData() {
        self.filters.removeAll()
    }
    
    func getFilterData() -> [FilterCategory] {
        let allApiFilterOptions = NetworkServiceConstantsEnums.FilterOptions.allCases
        
        return allApiFilterOptions.compactMap { apiFilterOption in
            guard let filterOption = LaunchesRepositoryEnums.Filters.Options.from(apiFilterOption: apiFilterOption) else { return nil }
            var filters = [String]()
            
            switch apiFilterOption {
            case .Success:
                filters = filters + self.getSuccessFilterData()
            case .DateUTC:
                filters = getLaunchYears()
            }
            
            if filters.isEmpty {
                return nil
            }
            
            return FilterCategory(option: filterOption, filters: filters)
        }
    }
    
    func addFilter(_ filter: LaunchesRepositoryEnums.Filters.Options, selectedValues: [String]) {
        let filterOption = FilterCategory(option: filter, filters: selectedValues)
        
        self.filters.append(filterOption)
    }
    
    // MARK: -
    // MARK: PRIVATE FUNCTIONS
    
    func getLaunchYears() -> [String] {
        if self.filterLaunchYears.isEmpty {
            let launchYears = self.launches.compactMap({(launch: Launch) -> String? in
                guard let launchDate = launch.launchDate else { return nil }
                
                return launchDate.toYearOnlyDisplayString
            })
            
            self.filterLaunchYears = Array(Set(launchYears)).sorted()
        }
        
        return self.filterLaunchYears
    }
    
    func getSuccessFilterData() -> [String] {
        let allApiOptions = NetworkServiceConstantsEnums.SuccessFilters.allCases
        let rawValues = allApiOptions.map { $0.rawValue }
        
        return rawValues
    }
    
    // MARK: -
    // MARK: MAPPING FUNCTIONS
    
    func mapFiltersToQueryOptions() -> [QueryOption]? {
        return self.filters.compactMap({(filter: FilterCategory) -> QueryOption? in
            return self.mapFilterToQueryOption(filter)
        })
    }
    
    func mapFilterToQueryOption(_ filter: FilterCategory) -> QueryOption? {
        switch filter.option {
        case .LaunchYear:
            return LaunchYearQueryOption(with: filter.filters)
        case .SuccessfulLaunch:
            let successFilter = filter.filters.compactMap { NetworkServiceConstantsEnums.SuccessFilters.init(rawValue:$0) }
            
            return successFilter.isEmpty ? nil : SuccessfulLaunchQueryOption(with: successFilter)
        }
    }
    
}
