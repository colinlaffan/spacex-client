//
//  LaunchesRepositoryProtocol.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import Foundation

protocol LaunchesRepositoryProtocol {
    
    init(with networkService: NetworkServiceProtocol)
    
    func fetchAllLaunches(callback: @escaping (Bool) -> Void)
    func getLaunches() -> [Launch]
    func getRawSortOrder() -> String
    func setSortOrder(_ selectedSort: String)
    func getFilterData() -> [FilterCategory]
    func clearFilterData()
    func addFilter(_ filter: LaunchesRepositoryEnums.Filters.Options, selectedValues: [String])
    
}
