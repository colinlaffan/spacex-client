//
//  NetworkService.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import Foundation

class NetworkService: NetworkServiceProtocol {
    
    // MARK: -
    // MARK: INJECTED PROPERTIES
    
    let configuration: NetworkServiceConfiguration
    
    // MARK: -
    // MARK: PRIVATE PROPERTIES

    let session: URLSession
    
    // MARK: -
    // MARK: PROTOCOL IMPLEMENTATION
    
    required init(with configuration: NetworkServiceConfiguration) {
        let sessionConfiguration = URLSessionConfiguration.default

        sessionConfiguration.timeoutIntervalForRequest = TimeInterval(configuration.timeout)

        self.session = URLSession(configuration: sessionConfiguration)
        self.configuration = configuration
    }
    
    func load<Resource>(using resource: Resource, completion: @escaping (Resource.Response?, NetworkServiceConstantsEnums.errors?) -> Void) where Resource : NetworkResource {
        let urlRequest = buildRequest(for: resource)
        let task = self.session.dataTask(with: urlRequest) { data, response, error in
            if let networkError = self.responseError(response: response as? HTTPURLResponse, error: error, data: data) {
                completion(nil, networkError)
            } else {
                if let data = data {
                    do {
                        let apiResponse = try JSONDecoder().decode(Resource.Response.self, from: data)

                        completion(apiResponse, nil)
                    } catch {
                        print("Unexpected error: \(error)")
                        completion(nil, .UnableToDecode)
                    }
                } else {
                    completion(nil, .NoData)
                }
            }
        }

        task.resume()
    }
    
}

extension NetworkService {
    
    // MARK: -
    // MARK: PRIVATE FUNCTIONS
    
    func buildRequest<T: NetworkResource>(for resource: T) -> URLRequest {
        let urlString = self.configuration.baseUrl + resource.path
        let url = URL(string: urlString)!
        var urlRequest = URLRequest(url: url)

        urlRequest.httpMethod = resource.httpMethod.rawValue
        urlRequest.httpBody = resource.httpBody

        if let resourceHeaders = resource.headers {
            for (key, value) in resourceHeaders {
                urlRequest.addValue(value, forHTTPHeaderField: key)
            }
        }
        
        return urlRequest
    }
    
    func responseError(response: HTTPURLResponse?, error: Error?, data: Data?) -> NetworkServiceConstantsEnums.errors? {
        if let error = error as NSError? {
            switch URLError.Code(rawValue: error.code) {
            case .notConnectedToInternet:
                return .NoConnection
            default:
                return .Server
            }
        } else {
            if let response = response {
                switch response.statusCode {
                case 200...299:
                    return nil
                case 400:
                    return .BadRequest
                case 401:
                    return .Unauthorized
                case 404:
                    return .NotFound
                default:
                    return .Server
                }
            } else {
                return .Server
            }
        }
    }
    
}
