//
//  NetworkServiceProtocol.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import Foundation

protocol NetworkServiceProtocol {
    
    var configuration: NetworkServiceConfiguration { get }
    
    init(with configuration: NetworkServiceConfiguration)
    
    func load<Resource: NetworkResource>(using resource: Resource, completion: @escaping (Resource.Response?, NetworkServiceConstantsEnums.errors?) -> Void)
    
}
