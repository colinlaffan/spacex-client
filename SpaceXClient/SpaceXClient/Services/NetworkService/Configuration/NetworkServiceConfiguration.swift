//
//  NetworkServiceConfiguration.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import Foundation

struct NetworkServiceConfiguration {
    
    // MARK: -
    // MARK: INJECTED PROPERTIES

    let baseUrl: String
    let timeout: Int
    
}
