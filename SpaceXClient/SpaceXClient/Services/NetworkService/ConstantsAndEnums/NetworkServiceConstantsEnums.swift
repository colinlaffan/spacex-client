//
//  NetworkServiceConstantsEnums.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import Foundation

struct NetworkServiceConstantsEnums {
    
    struct Requests {
        
        enum HeaderKey: String {
            case ContentType = "Content-Type"
        }
        
        enum HeaderValue: String {
            case ApplicationJson = "application/json"
        }
        
    }
    
    enum httpMethods: String {
        case GET
        case POST
    }
    
    enum paths: String {
        // Launches
        case PostAllLaunches = "launches/query"
        // Company
        case GetAllCompanyInfo = "company"
        // Rockets
        case GetAllRockets = "rockets"
    }
    
    enum errors: Error {
        case NoConnection
        case Unauthorized
        case NotFound
        case BadRequest
        case Server
        case UnableToDecode
        case NoData
    }
    
    enum SortKeys: String {
        case utcDate = "date_utc"
    }
    
    enum SortOrder: String {
        case Ascending = "asc"
        case Descending = "desc"
    }
    
    enum FilterOptions: String, CaseIterable {
        case Success = "success"
        case DateUTC = "date_utc"
    }
    
    enum SuccessFilters: String, CaseIterable {
        case True = "Yes"
        case False = "No"
        
        var boolValue: Bool {
            switch self {
            case .True:
                return true
            case .False:
                return false
            }
        }
    }
    
}
