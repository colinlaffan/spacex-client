//
//  OptionsDTO.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 14/02/2022.
//

import Foundation

struct OptionsDTO: Codable {
    
    let pagination: Bool
    let populate: [String]
    let sort: SortDTO
    
}
