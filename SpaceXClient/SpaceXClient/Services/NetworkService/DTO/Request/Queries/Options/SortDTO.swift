//
//  SortDTO.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 14/02/2022.
//

import Foundation

struct SortDTO: Codable {
    
    let dateUTC: String

    enum CodingKeys: String, CodingKey {
        case dateUTC = "date_utc"
    }
    
}
