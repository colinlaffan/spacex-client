//
//  QueriesDTO.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 14/02/2022.
//

import Foundation

struct QueriesDTO: Codable {
    
    let query: QueryDTO?
    let options: OptionsDTO
    
}
