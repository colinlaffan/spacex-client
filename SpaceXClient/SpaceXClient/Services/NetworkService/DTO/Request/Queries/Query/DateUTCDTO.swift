//
//  DateUTCDTO.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 14/02/2022.
//

import Foundation

struct DateUTCDTO: Codable {
    let greaterThan, lessThan: String

    enum CodingKeys: String, CodingKey {
        case greaterThan = "$gte"
        case lessThan = "$lte"
    }
}
