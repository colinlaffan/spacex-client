//
//  OrDTO.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 14/02/2022.
//

import Foundation

struct OrDTO: Codable {
    let dateUTC: DateUTCDTO

    enum CodingKeys: String, CodingKey {
        case dateUTC = "date_utc"
    }
}
