//
//  QueryDTO.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 14/02/2022.
//

import Foundation

struct QueryDTO: Codable {
    let or: [OrDTO]?
    let success: SuccessDTO?

    enum CodingKeys: String, CodingKey {
        case or = "$or"
        case success
    }
}
