//
//  SuccessDTO.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 14/02/2022.
//

import Foundation

struct SuccessDTO: Codable {
    let successIn: [Bool]

    enum CodingKeys: String, CodingKey {
        case successIn = "$in"
    }
}
