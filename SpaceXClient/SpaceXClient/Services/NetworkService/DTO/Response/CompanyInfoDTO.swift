//
//  CompanyInfoDTO.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import Foundation

struct CompanyInfoDTO: Decodable {
    
    let name: String
    let founder: String
    let founded: Int
    let employees: Int
    let launchSites: Int
    let ctoPropulsion: String
    let valuation: Int
    
    private enum CodingKeys: String, CodingKey {
        case name
        case founder
        case founded
        case employees
        case launchSites = "launch_sites"
        case ctoPropulsion = "cto_propulsion"
        case valuation
    }
    
}
