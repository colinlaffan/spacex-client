//
//  LaunchDTO.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 09/02/2022.
//

import Foundation

struct LaunchDTO: Decodable {
    
    let name: String
    let dateUTC: Date?
    let success: Bool?
    let rocket: RocketDTO?
    let links: LaunchLinkDTO?
    
    private enum CodingKeys: String, CodingKey {
        case name
        case dateUTC = "date_utc"
        case rocket
        case success
        case links
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        
        if let value = try container.decodeIfPresent(String.self, forKey: .dateUTC) {
            dateUTC = Date.from(isoAPIValue: value)
        } else {
            dateUTC = nil
        }
        
        success = try container.decodeIfPresent(Bool.self, forKey: .success)
        rocket = try container.decodeIfPresent(RocketDTO.self, forKey: .rocket)
        links = try container.decodeIfPresent(LaunchLinkDTO.self, forKey: .links)
    }
    
}

struct LaunchLinkDTO: Decodable {
    
    let patch: LinkPatchDTO?
    
}

struct LinkPatchDTO: Decodable {
 
    let small: String?
    let large: String?
    
}
