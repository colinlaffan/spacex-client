//
//  PostLaunchesWithQueryResponseDTO.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 11/02/2022.
//

import Foundation

struct PostLaunchesWithQueryResponseDTO: Decodable {
    
    let launhes: [LaunchDTO]?
    
    private enum CodingKeys: String, CodingKey {
        case launhes = "docs"
    }
    
}
