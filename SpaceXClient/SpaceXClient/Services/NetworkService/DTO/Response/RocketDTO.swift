//
//  RocketDTO.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 10/02/2022.
//

import Foundation

struct RocketDTO: Decodable {
    
    let id: String
    let name: String
    let type: String
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case type
    }
    
}
