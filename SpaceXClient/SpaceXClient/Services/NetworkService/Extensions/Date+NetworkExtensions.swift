//
//  Date+NetworkExtensions.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 14/02/2022.
//

import Foundation

extension Date {
    
    static func start(of year: String) -> Date? {
        guard let year = Int(year) else { return nil }
        let components = DateComponents(year: year, month: 1, day: 1)
        
        return Calendar.current.date(from: components)
    }
    
    static func end(of year: String) -> Date? {
        guard let year = Int(year) else { return nil }
        let components = DateComponents(year: year, month: 12, day: 31)
        
        return Calendar.current.date(from: components)
    }
    
    func toISOFormattedString() -> String {
        let dateFormatter = DateFormatter()

        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")

        return dateFormatter.string(from: self)
    }
    
}
