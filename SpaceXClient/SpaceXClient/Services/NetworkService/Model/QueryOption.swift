//
//  QueryOption.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 14/02/2022.
//

import Foundation

protocol QueryOption {
    
    var queryType: NetworkServiceConstantsEnums.FilterOptions { get }
    
}

struct LaunchYearQueryOption: QueryOption {
    
    var queryType: NetworkServiceConstantsEnums.FilterOptions {
        return .DateUTC
    }
    
    let dates: [LaunchYearDates]?
    
    init?(with years: [String]) {
        let dates = years.compactMap { LaunchYearDates(with: $0) }
        
        if dates.isEmpty {
            return nil
        }
        
        self.dates = dates
    }
    
}

struct LaunchYearDates {
    
    let startDate: Date?
    let endDate: Date?
    
    init?(with year: String) {
        guard let startDate = Date.start(of: year), let endDate = Date.end(of: year) else { return nil }
        
        self.startDate = startDate
        self.endDate = endDate
    }
    
}

struct SuccessfulLaunchQueryOption: QueryOption {
    
    var queryType: NetworkServiceConstantsEnums.FilterOptions {
        return .Success
    }
    
    let values: [NetworkServiceConstantsEnums.SuccessFilters]
    
    init(with values: [NetworkServiceConstantsEnums.SuccessFilters]) {
        self.values = values
    }
    
}
