//
//  SortOption.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 11/02/2022.
//

import Foundation

struct SortOption {
    
    let key: String
    let order: NetworkServiceConstantsEnums.SortOrder
    
}
