//
//  GetAllCompanyInfoResource.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import Foundation

struct GetAllCompanyInfoResource: NetworkResource {
    
    // MARK: -
    // MARK: PROTOCOL IMPLEMENTATION
    
    typealias Response = CompanyInfoDTO
    
    var path: String {
        return NetworkServiceConstantsEnums.paths.GetAllCompanyInfo.rawValue
    }
    var httpMethod: NetworkServiceConstantsEnums.httpMethods {
        return .GET
    }
    var httpBody: Data? {
        return nil
    }
    var headers: [String: String]? {
        return nil
    }
    
}
