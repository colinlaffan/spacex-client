//
//  GetAllLaunchesResource.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 09/02/2022.
//

import Foundation

struct PostAllLaunchesResource: NetworkResource {
    
    // MARK: -
    // MARK: PROTOCOL IMPLEMENTATION
    
    typealias Response = PostLaunchesWithQueryResponseDTO
    
    var path: String {
        return NetworkServiceConstantsEnums.paths.PostAllLaunches.rawValue
    }
    var httpMethod: NetworkServiceConstantsEnums.httpMethods {
        return .POST
    }
    var httpBody: Data? {
        let queryDTO = self.mapQueryOptionsToQueryDTO(self.queryOptions)
        let optionsDTO = self.mapSortOrderToOptionsDTO(self.sortOrder)
        let queriesDTO = QueriesDTO(query: queryDTO, options: optionsDTO)

        return try? JSONEncoder().encode(queriesDTO)
    }
    var headers: [String: String]? {
        return [NetworkServiceConstantsEnums.Requests.HeaderKey.ContentType.rawValue: NetworkServiceConstantsEnums.Requests.HeaderValue.ApplicationJson.rawValue]
    }
    
    // MARK: -
    // MARK: INJECTED PARAMETERS

    let sortOrder: NetworkServiceConstantsEnums.SortOrder
    let queryOptions: [QueryOption]?

    // MARK: -
    // MARK: INITIALIZATION

    init(with sortOrder: NetworkServiceConstantsEnums.SortOrder, and queryOptions: [QueryOption]?) {
        self.sortOrder = sortOrder
        self.queryOptions = queryOptions
    }
    
    // MARK: -
    // MARK: MAPPER FUNCTIONS
    
    func mapSortOrderToOptionsDTO(_ sortOrder: NetworkServiceConstantsEnums.SortOrder) -> OptionsDTO {
        let sortDTO = SortDTO(dateUTC: sortOrder.rawValue)
        
        return OptionsDTO(pagination: false, populate: ["rocket"], sort: sortDTO)
    }
    
    func mapQueryOptionsToQueryDTO(_ queryOptions: [QueryOption]?) -> QueryDTO? {
        guard let queryOptions = queryOptions else { return nil }
        let successQueryOption = queryOptions.first(where: {($0 is SuccessfulLaunchQueryOption)}) as? SuccessfulLaunchQueryOption
        let successDTO = self.mapSuccessQueryOptionToSuccessDTO(successQueryOption)
        let launchDateQueryOption = queryOptions.first(where: {($0 is LaunchYearQueryOption)}) as? LaunchYearQueryOption
        let orDTO = self.mapLaunchDateQueryOptionToOrDTOs(launchDateQueryOption)
        
        return QueryDTO(or: orDTO, success: successDTO)
    }
    
    func mapSuccessQueryOptionToSuccessDTO(_ successQueryOption: SuccessfulLaunchQueryOption?) -> SuccessDTO? {
        guard let successQueryOption = successQueryOption else { return nil }
        let successInValues = successQueryOption.values.map { $0.boolValue }
        
        return SuccessDTO(successIn: successInValues)
    }
    
    func mapLaunchDateQueryOptionToOrDTOs(_ launchDateQueryOption: LaunchYearQueryOption?) -> [OrDTO]? {
        guard let launchDateQueryOption = launchDateQueryOption, let launchDates = launchDateQueryOption.dates else { return nil }
        let orDTOValues = launchDates.compactMap({ (launchDate: LaunchYearDates) -> OrDTO? in
            guard let greaterThan = launchDate.startDate?.toISOFormattedString(), let lessThan = launchDate.endDate?.toISOFormattedString() else { return nil }
            let dateUtcDTO = DateUTCDTO(greaterThan: greaterThan, lessThan: lessThan)
            
            return OrDTO(dateUTC: dateUtcDTO)
        })
        
        return orDTOValues
    }
}
