//
//  NetworkResource.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import Foundation

protocol NetworkResource {

    associatedtype Response: Decodable

    var path: String { get }
    var httpMethod: NetworkServiceConstantsEnums.httpMethods { get }
    var httpBody: Data? { get }
    var headers: [String: String]? { get }

}
