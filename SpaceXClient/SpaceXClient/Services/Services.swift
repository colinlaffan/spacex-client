//
//  Services.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 08/02/2022.
//

import Foundation

class Services {
    
    // MARK: -
    // MARK: INJECTED PROPERTIES
    
    let networkService: NetworkServiceProtocol
    let companyRepository: CompanyRepositoryProtocol
    let launchesRepository: LaunchesRepositoryProtocol
    
    // MARK: -
    // MARK: INITIALIZATION
    
    init(with networkService: NetworkServiceProtocol,
         companyRepository: CompanyRepositoryProtocol,
         launchesRepository: LaunchesRepositoryProtocol) {
        self.networkService = networkService
        self.companyRepository = companyRepository
        self.launchesRepository = launchesRepository
    }
    
}
