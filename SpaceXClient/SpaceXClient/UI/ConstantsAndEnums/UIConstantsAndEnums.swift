//
//  UIConstantsAndEnums.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 09/02/2022.
//

import Foundation

struct UIConstantsAndEnums {
    
    struct Cells {
        
        enum CellType {
            case HeadingOneLabel
            case ListSummary
            case DetailsCard
            case Selectable
        }
        
        struct DetailsCard {
            
            enum RightImageName: String {
                case Checkmark = "checkmark"
                case Xmark = "xmark"
            }
            
        }
        
    }
    
}
