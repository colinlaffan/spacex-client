//
//  CellData.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 09/02/2022.
//

import Foundation

struct CellData {
    
    var cellType = UIConstantsAndEnums.Cells.CellType.HeadingOneLabel
    
    // MARK: -
    // MARK: TEXT PROPERTIES
    
    var titleText: String? = nil
    var detailText1: String? = nil
    var detailText2: String? = nil
    var detailText3: String? = nil
    var detailText4: String? = nil
    
    // MARK: -
    // MARK: LABEL PROPERTIES
    
    var labelText1: String? = nil
    var labelText2: String? = nil
    var labelText3: String? = nil
    var labelText4: String? = nil
    
    // MARK: -
    // MARK: BOOL PROPERTIES
    
    var detailBool1: Bool? = nil
    
    // MARK: -
    // MARK: IMAGE PROPERTIES
    
    var isImageHidden: Bool? = nil
    var imageUrlPath: String? = nil
    
    // MARK: -
    // MARK: CUSTOM INITIALIZER
    
    public init(with cellType: UIConstantsAndEnums.Cells.CellType) {
        self.cellType = cellType
    }
    
}
