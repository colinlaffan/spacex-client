//
//  DetailsCardTableViewCell.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 09/02/2022.
//

import UIKit
import AlamofireImage

class DetailsCardTableViewCell: UITableViewCell {

    // MARK: -
    // MARK: VIEW OUTLETS
    
    @IBOutlet weak var missionPatchImageView: UIImageView!
    @IBOutlet weak var successfulLaunchImageView: UIImageView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var valueLabel1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var valueLabel2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var valueLabel3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var valueLabel4: UILabel!
    
    // MARK: -
    // MARK: COMPUTED PROPERTIES
    var leftPlaceholderImage: UIImage? {
        return UIImage(systemName: "arrow.down.to.line.circle")
    }
    
    // MARK: -
    // MARK: SETUP

    public func setup(with viewModel: DetailsCardCellViewModel) {
        if let imageUrl = URL(string: viewModel.leftImageUrlPath) {
            self.missionPatchImageView.af.setImage(withURL: imageUrl, placeholderImage: self.leftPlaceholderImage)
        }
        
        self.label1.text = viewModel.label1Text
        self.valueLabel1.text = viewModel.valueLabel1Text
        self.label2.text = viewModel.label2Text
        self.valueLabel2.text = viewModel.valueLabel2Text
        self.label3.text = viewModel.label3Text
        self.valueLabel3.text = viewModel.valueLabel3Text
        self.label4.text = viewModel.label4Text
        self.valueLabel4.text = viewModel.valueLabel4Text
        self.successfulLaunchImageView.image = UIImage(systemName: viewModel.rightImageName)
    }
    
    override func prepareForReuse() {
        self.missionPatchImageView.image = nil
        self.successfulLaunchImageView.image = nil
    }
    
}

extension DetailsCardTableViewCell {
    
    static func with(viewModel: CellViewModel, tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel as? DetailsCardCellViewModel,
                let cell = tableView.dequeueReusableCell(of: DetailsCardTableViewCell.self, for: indexPath)
        else {
            return UITableViewCell()
        }
        
        cell.setup(with: viewModel)
        
        return cell
    }
    
}
