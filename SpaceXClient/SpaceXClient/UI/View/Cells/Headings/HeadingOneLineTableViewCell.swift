//
//  HeadingOneLineTableViewCell.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 09/02/2022.
//

import UIKit

class HeadingOneLineTableViewCell: UITableViewCell {

    // MARK: -
    // MARK: VIEW OUTLETS
    
    @IBOutlet weak var label: UILabel!
    
    // MARK: -
    // MARK: SETUP

    public func setup(with viewModel: HeadingOneLineCellViewModel) {
        self.label.text = viewModel.labelText
    }
    
}

extension HeadingOneLineTableViewCell {
    
    static func with(viewModel: CellViewModel, tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel as? HeadingOneLineCellViewModel,
                let cell = tableView.dequeueReusableCell(of: HeadingOneLineTableViewCell.self, for: indexPath)
        else {
            return UITableViewCell()
        }
        
        cell.setup(with: viewModel)
        
        return cell
    }
    
}
