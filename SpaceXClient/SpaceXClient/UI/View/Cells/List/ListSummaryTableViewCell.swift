//
//  ListSummaryTableViewCell.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 09/02/2022.
//

import UIKit

class ListSummaryTableViewCell: UITableViewCell {

    // MARK: -
    // MARK: VIEW OUTLETS
    
    @IBOutlet weak var label: UILabel!
    
    // MARK: -
    // MARK: SETUP

    public func setup(with viewModel: ListSummaryCellViewModel) {
        self.label.text = viewModel.labelText
    }
    
}

extension ListSummaryTableViewCell {
    
    static func with(viewModel: CellViewModel, tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel as? ListSummaryCellViewModel,
                let cell = tableView.dequeueReusableCell(of: ListSummaryTableViewCell.self, for: indexPath)
        else {
            return UITableViewCell()
        }
        
        cell.setup(with: viewModel)
        
        return cell
    }
    
}

