//
//  SelectableTableViewCell.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 13/02/2022.
//

import UIKit

class SelectableTableViewCell: UITableViewCell {

    // MARK: -
    // MARK: VIEW OUTLETS
    
    @IBOutlet weak var isSelectedImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    // MARK: -
    // MARK: PROPERTIES
    
    var viewModel: SelectableCellViewModel?
    
    // MARK: -
    // MARK: VIEW LIFECYCLE
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.viewModel?.isSelected.listener = nil
    }
    
    // MARK: -
    // MARK: SETUP

    public func setup(with viewModel: SelectableCellViewModel) {
        self.viewModel = viewModel
        self.titleLabel.text = viewModel.titleText
        self.bindViewModel()
    }
    
    // MARK: -
    // MARK: BINDED FUNCTIONS
    
    func bindViewModel() {
        self.viewModel?.isSelected.bindAndFire { [weak self] isSelected in
            self?.isSelectedImageView.isHidden = !isSelected
        }
    }
    
}

extension SelectableTableViewCell {
    
    static func with(viewModel: CellViewModel, tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel as? SelectableCellViewModel,
                let cell = tableView.dequeueReusableCell(of: SelectableTableViewCell.self, for: indexPath)
        else {
            return UITableViewCell()
        }
        
        cell.setup(with: viewModel)
        
        return cell
    }
    
}
