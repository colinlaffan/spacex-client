//
//  CellsViewModel.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 09/02/2022.
//

import Foundation

protocol CellViewModel {
    
    var cellType: UIConstantsAndEnums.Cells.CellType { get }
    
}
