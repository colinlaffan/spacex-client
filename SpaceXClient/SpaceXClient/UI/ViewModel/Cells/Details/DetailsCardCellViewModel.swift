//
//  DetailsCardCellViewModel.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 09/02/2022.
//

import Foundation

struct DetailsCardCellViewModel: CellViewModel {
    
    // MARK: -
    // MARK: INJECTED DEPENDENCIES
    
    let model: CellData
    
    // MARK: -
    // MARK: PROTOCOL PROPERTIES
    
    var cellType: UIConstantsAndEnums.Cells.CellType {
        return self.model.cellType
    }
    
    // MARK: -
    // MARK: COMPUTED PROPERTIES
    
    var leftImageUrlPath: String {
        return self.model.imageUrlPath ?? ""
    }
    var label1Text: String {
        return self.model.labelText1 ?? ""
    }
    var valueLabel1Text: String {
        return self.model.detailText1 ?? ""
    }
    var label2Text: String {
        return self.model.labelText2 ?? ""
    }
    var valueLabel2Text: String {
        return self.model.detailText2 ?? ""
    }
    var label3Text: String {
        return self.model.labelText3 ?? ""
    }
    var valueLabel3Text: String {
        return self.model.detailText3 ?? ""
    }
    var label4Text: String {
        return self.model.labelText4 ?? ""
    }
    var valueLabel4Text: String {
        return self.model.detailText4 ?? ""
    }
    var rightImageName: String {
        return self.model.detailBool1 == true ? UIConstantsAndEnums.Cells.DetailsCard.RightImageName.Checkmark.rawValue : UIConstantsAndEnums.Cells.DetailsCard.RightImageName.Xmark.rawValue
    }
    
    // MARK: -
    // MARK: INITIALIZATION
    
    public init(with model: CellData) {
        self.model = model
    }
    
}
