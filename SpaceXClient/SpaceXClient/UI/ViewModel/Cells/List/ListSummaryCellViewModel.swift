//
//  ListSummaryCellViewModel.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 09/02/2022.
//

import Foundation

struct ListSummaryCellViewModel: CellViewModel {
    
    // MARK: -
    // MARK: INJECTED DEPENDENCIES
    
    let model: CellData
    
    // MARK: -
    // MARK: PROTOCOL PROPERTIES
    
    var cellType: UIConstantsAndEnums.Cells.CellType {
        return self.model.cellType
    }
    
    // MARK: -
    // MARK: COMPUTED PROPERTIES
    
    var labelText: String {
        return self.model.titleText ?? ""
    }
    
    // MARK: -
    // MARK: INITIALIZATION
    
    public init(with model: CellData) {
        self.model = model
    }
    
}
