//
//  SelectableCellViewModel.swift
//  SpaceXClient
//
//  Created by Colin Laffan on 13/02/2022.
//

import Foundation

struct SelectableCellViewModel: CellViewModel {
    
    // MARK: -
    // MARK: INJECTED DEPENDENCIES
    
    let model: CellData
    
    // MARK: -
    // MARK: PROTOCOL PROPERTIES
    
    var cellType: UIConstantsAndEnums.Cells.CellType {
        return self.model.cellType
    }
    
    // MARK: -
    // MARK: COMPUTED PROPERTIES
    
    var titleText: String {
        return self.model.titleText ?? ""
    }
    
    // MARK: BINDED PROPERTIES

    let isSelected: DynamicProperty<Bool> = DynamicProperty(false)
    
    // MARK: -
    // MARK: INITIALIZATION
    
    public init(with model: CellData) {
        self.model = model
    }
    
    func didSelectCell() {
        self.isSelected.value = !self.isSelected.value
    }
    
}
