//
//  CompanyInfoViewModelTests.swift
//  SpaceXClientTests
//
//  Created by Colin Laffan on 15/02/2022.
//

import XCTest
@testable import SpaceXClient

class CompanyInfoViewModelTests: XCTestCase {

    // MARK: -
    // MARK: setup TESTS
    
    var networkServiceConfiguration: NetworkServiceConfiguration!
    var mockNetworkService: MockNetworkService!
    var mockCompanyRepository: MockCompanyRepository!
    var mockLaunchesRepository: MockLaunchesRepository!
    
    override func setUp() {
        networkServiceConfiguration = NetworkServiceConfiguration(baseUrl: "", timeout: 30)
        mockNetworkService = MockNetworkService(with: networkServiceConfiguration)
        mockCompanyRepository = MockCompanyRepository(with: mockNetworkService)
        mockLaunchesRepository = MockLaunchesRepository(with: mockNetworkService)
    }
    
    // MARK: -
    // MARK: BINDED CLOSURES TESTS
    
    func testHandleLoadingError_WithDefaultValues_BindedClosuresCalled() {
        // Arrange
        let sut = CompanyInfoViewModel(with: mockCompanyRepository, launchesRepository: mockLaunchesRepository)
        var testWasHideLoadingActivityCalled = false
        var testNoContentTitleTextValue = ""
        var testNoContentSubTitleTextValue = ""
        var testIsTableViewHiddenValue = false
        var testIsNoContentContainerViewHiddenValue = true
        
        sut.hideLoadingActivity = {
            testWasHideLoadingActivityCalled = true
        }
        sut.noContentTitleText.bindAndFire {
            testNoContentTitleTextValue = $0
        }
        sut.noContentSubTitleText.bindAndFire {
            testNoContentSubTitleTextValue = $0
        }
        sut.isTableViewHidden.bindAndFire {
            testIsTableViewHiddenValue = $0
        }
        sut.isNoContentContainerViewHidden.bindAndFire {
            testIsNoContentContainerViewHiddenValue = $0
        }

        // Act
        sut.handleLoadingError()

        // Assert
        XCTAssertTrue(testWasHideLoadingActivityCalled)
        XCTAssertEqual(CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.NoContentErrorTitle.rawValue, testNoContentTitleTextValue)
        XCTAssertEqual(CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.NoContentErrorSubTitle.rawValue, testNoContentSubTitleTextValue)
        XCTAssertTrue(testIsTableViewHiddenValue)
        XCTAssertFalse(testIsNoContentContainerViewHiddenValue)
    }

    // MARK: -
    // MARK: POPULATE CELL VIEW MODELS TESTS
    
    func testCompanyHeaderViewModel_WithDefaultValues_ShouldReturnMatchingViewModel() {
        // Arrange
        let sut = CompanyInfoViewModel(with: mockCompanyRepository, launchesRepository: mockLaunchesRepository)

        // Act
        let result = sut.companyHeaderViewModel()

        // Assert
        XCTAssertEqual(.HeadingOneLabel, result.cellType)
        
        guard let stronlgyTypedViewModel = result as? HeadingOneLineCellViewModel else {
            XCTFail("testCompanyHeaderViewModel_WithDefaultValues_ShouldReturnMatchingViewModel - Type mismatch")
            return
        }
        
        XCTAssertEqual(CompanyInfoConstantsAndEnums.CompanyInfoView.Constants.HeadingCompany.rawValue, stronlgyTypedViewModel.labelText)
    }
    
}
