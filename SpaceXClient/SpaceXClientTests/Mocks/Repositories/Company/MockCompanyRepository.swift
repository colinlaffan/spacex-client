//
//  MockCompanyRepository.swift
//  SpaceXClientTests
//
//  Created by Colin Laffan on 15/02/2022.
//

import Foundation
@testable import SpaceXClient

class MockCompanyRepository: CompanyRepositoryProtocol {
    
    required init(with networkService: NetworkServiceProtocol) {
        
    }
    
    func fetchAllCompanyInfo(callback: @escaping (Bool) -> Void) {
        
    }
    
    func getCompany() -> Company? {
        return nil
    }
    
    
}
