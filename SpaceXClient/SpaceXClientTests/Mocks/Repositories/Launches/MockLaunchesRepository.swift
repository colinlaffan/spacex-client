//
//  MockLaunchesRepository.swift
//  SpaceXClientTests
//
//  Created by Colin Laffan on 15/02/2022.
//

import Foundation
@testable import SpaceXClient

class MockLaunchesRepository: LaunchesRepositoryProtocol {
    
    required init(with networkService: NetworkServiceProtocol) {
        
    }
    
    func fetchAllLaunches(callback: @escaping (Bool) -> Void) {
        
    }
    
    func getLaunches() -> [Launch] {
        return [Launch]()
    }
    
    func getRawSortOrder() -> String {
        return ""
    }
    
    func setSortOrder(_ selectedSort: String) {
        
    }
    
    func getFilterData() -> [FilterCategory] {
        return [FilterCategory]()
    }
    
    func clearFilterData() {
        
    }
    
    func addFilter(_ filter: LaunchesRepositoryEnums.Filters.Options, selectedValues: [String]) {
        
    }
    
}
