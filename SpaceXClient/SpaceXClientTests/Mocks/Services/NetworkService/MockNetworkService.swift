//
//  MockNetworkService.swift
//  SpaceXClientTests
//
//  Created by Colin Laffan on 14/02/2022.
//

import Foundation
@testable import SpaceXClient

enum MockNetworkServiceLoadResponse {
    case FetchAllCompanyInfoResponseSuccess
    case FetchAllCompanyInfoResponseFailure
}

class MockNetworkService: NetworkServiceProtocol {
    
    // MARK: -
    // MARK: MOCK PROPERTIES
    
    var loadReponse = MockNetworkServiceLoadResponse.FetchAllCompanyInfoResponseSuccess
    
    // MARK: -
    // MARK: PRIVATE PROPERTIES
    
    var companyInfoDTO: CompanyInfoDTO {
        return CompanyInfoDTO(name: "SpaceX", founder: "Elon Musk", founded: 2002, employees: 8000, launchSites: 3, ctoPropulsion: "Tom Mueller", valuation: 52000000000)
    }
    
    // MARK: -
    // MARK: PROTOCOL IMPLEMENTATION
    
    var configuration: NetworkServiceConfiguration
    
    required init(with configuration: NetworkServiceConfiguration) {
        self.configuration = configuration
    }
    
    func load<Resource>(using resource: Resource, completion: @escaping (Resource.Response?, NetworkServiceConstantsEnums.errors?) -> Void) where Resource : NetworkResource {
        switch self.loadReponse {
        case .FetchAllCompanyInfoResponseSuccess:
            completion(self.companyInfoDTO as? Resource.Response, nil)
        case .FetchAllCompanyInfoResponseFailure:
            completion(nil, .Server)
        }
    }
    
    
}
