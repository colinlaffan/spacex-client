//
//  CompanyRepositoryTests.swift
//  SpaceXClientTests
//
//  Created by Colin Laffan on 14/02/2022.
//

import XCTest
@testable import SpaceXClient

class CompanyRepositoryTests: XCTestCase {

    var companyInfoDTO: CompanyInfoDTO {
        return CompanyInfoDTO(name: "SpaceX", founder: "Elon Musk", founded: 2002, employees: 8000, launchSites: 3, ctoPropulsion: "Tom Mueller", valuation: 52000000000)
    }
    var company: Company {
        return Company(with: self.companyInfoDTO)
    }
    
    // MARK: -
    // MARK: setup TESTS
    
    var networkServiceConfiguration: NetworkServiceConfiguration!
    var mockNetworkService: MockNetworkService!
    
    override func setUp() {
        networkServiceConfiguration = NetworkServiceConfiguration(baseUrl: "", timeout: 30)
        mockNetworkService = MockNetworkService(with: networkServiceConfiguration)
    }
    
    // MARK: -
    // MARK: init TESTS
    
    func testCompanyRepository_Init_DefaultValuesShouldMatch() {
        // Arrange

        // Act
        let result = CompanyRepository(with: mockNetworkService)

        // Assert
        XCTAssertNil(result.company)
    }

    // MARK: -
    // MARK: getCompany TESTS
    
    func testGetCompany_WithCompanyPopulated_ValuesShouldMatch() {
        // Arrange
        let testCompany = company
        let sut = CompanyRepository(with: mockNetworkService)

        sut.company = testCompany
        
        // Act
        let result = sut.getCompany()

        // Assert
        XCTAssertNotNil(result)
        XCTAssertEqual(testCompany.name, result?.name)
        XCTAssertEqual(testCompany.founderName, result?.founderName)
        XCTAssertEqual(testCompany.yearFounded, result?.yearFounded)
        XCTAssertEqual(testCompany.numberOfEmployees, result?.numberOfEmployees)
        XCTAssertEqual(testCompany.numberOfLaunchSites, result?.numberOfLaunchSites)
        XCTAssertEqual(testCompany.valuation, result?.valuation)
    }
    
    // MARK: -
    // MARK: fetchAllCompanyInfo TESTS
    
    func testFetchAllCompanyInfo_WithNoResponseDataReturnedFromNetworkService_CallbackShouldReturnFalse() {
        // Arrange
        let sut = CompanyRepository(with: mockNetworkService)
        let testExpectation = XCTestExpectation(description: "testFetchAllCompanyInfo_WithNoResponseDataReturnedFromNetworkService_CallbackShouldReturnFalse - Call fetchAllCompanyInfo")
        var testResponse = true

        mockNetworkService.loadReponse = .FetchAllCompanyInfoResponseFailure
        
        // Act
        sut.fetchAllCompanyInfo { response in
            testResponse = response
            testExpectation.fulfill()
        }

        // Assert
        wait(for: [testExpectation], timeout: 2.0)
        XCTAssertFalse(testResponse)
        XCTAssertNil(sut.getCompany())
    }
    
    func testFetchAllCompanyInfo_WithResponseDataReturnedFromNetworkService_CallbackShouldReturnTrue() {
        // Arrange
        let sut = CompanyRepository(with: mockNetworkService)
        let testExpectation = XCTestExpectation(description: "testFetchAllCompanyInfo_WithResponseDataReturnedFromNetworkService_CallbackShouldReturnTrue - Call fetchAllCompanyInfo")
        var testResponse = false

        mockNetworkService.loadReponse = .FetchAllCompanyInfoResponseSuccess
        
        // Act
        sut.fetchAllCompanyInfo { response in
            testResponse = response
            testExpectation.fulfill()
        }

        // Assert
        wait(for: [testExpectation], timeout: 2.0)
        XCTAssertTrue(testResponse)
        XCTAssertNotNil(sut.getCompany())
    }
    
}
