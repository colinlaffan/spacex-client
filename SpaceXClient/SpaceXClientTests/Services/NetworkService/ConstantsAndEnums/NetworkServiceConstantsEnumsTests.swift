//
//  NetworkServiceConstantsEnumsTests.swift
//  SpaceXClientTests
//
//  Created by Colin Laffan on 14/02/2022.
//

import XCTest
@testable import SpaceXClient

class NetworkServiceConstantsEnumsTests: XCTestCase {

    // MARK: -
    // MARK: FilterOptions TESTS

    func testEnumFilterOptions_WithRawValues_CountAndValuesShouldMatch() {
        // Arrange

        // Act
        let result = NetworkServiceConstantsEnums.FilterOptions.allCases

        // Assert
        XCTAssertEqual(2, result.count)
        XCTAssertNotNil(result.first(where: { $0.rawValue == "success" }))
        XCTAssertNotNil(result.first(where: { $0.rawValue == "date_utc" }))
    }

    // MARK: -
    // MARK: SuccessFilters TESTS

    func testEnumSuccessFilters_WithRawValues_CountAndValuesShouldMatch() {
        // Arrange

        // Act
        let result = NetworkServiceConstantsEnums.SuccessFilters.allCases

        // Assert
        XCTAssertEqual(2, result.count)
        XCTAssertNotNil(result.first(where: { $0.rawValue == "Yes" }))
        XCTAssertNotNil(result.first(where: { $0.rawValue == "No" }))
    }
    
    func testEnumSuccessFilters_WithBoolValues_ValuesShouldMatch() {
        // Arrange

        // Act
        let resultTrue = NetworkServiceConstantsEnums.SuccessFilters.True
        let resultFalse = NetworkServiceConstantsEnums.SuccessFilters.False

        // Assert
        XCTAssertEqual(true, resultTrue.boolValue)
        XCTAssertEqual(false, resultFalse.boolValue)
    }
    
}
