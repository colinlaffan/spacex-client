//
//  CompanyInfoDTOTests.swift
//  SpaceXClientTests
//
//  Created by Colin Laffan on 14/02/2022.
//

import XCTest
@testable import SpaceXClient

class CompanyInfoDTOTests: XCTestCase {

    // MARK: -
    // MARK: CompanyInfoDTO JSON Decode TESTS
    
    func testCompanyInfoDTO_WithJSONData_ShouldReturnMatchingValues() {
        // Arrange
        let jsonFileLoader = JSONLoader()
        guard let jsonData = jsonFileLoader.load(fileName: .CompanyInfoDTOData) else {
            XCTFail("testCompanyInfoDTO_WithJSONData_ShouldReturnMatchingValues - Failed to load json file \(JsonFileNames.CompanyInfoDTOData.rawValue)")
            return
        }

        // Act
        guard let result = try? JSONDecoder().decode(CompanyInfoDTO.self, from: jsonData) else {
            XCTFail("testCompanyInfoDTO_WithJSONData_ShouldReturnMatchingValues - Failed to decode json file \(JsonFileNames.CompanyInfoDTOData.rawValue)")
            return
        }

        // Assert
        XCTAssertEqual("SpaceX", result.name)
        XCTAssertEqual("Elon Musk", result.founder)
        XCTAssertEqual(2002, result.founded)
        XCTAssertEqual(8000, result.employees)
        XCTAssertEqual(3, result.launchSites)
        XCTAssertEqual("Tom Mueller", result.ctoPropulsion)
        XCTAssertEqual(52000000000, result.valuation)
    }

}
