//
//  Date+NetworkExtensionsTests.swift
//  SpaceXClientTests
//
//  Created by Colin Laffan on 14/02/2022.
//

import XCTest
@testable import SpaceXClient

class Date_NetworkExtensionsTests: XCTestCase {

    // MARK: -
    // MARK: start:of TESTS

    func testStartOf_WithYearString_DateShouldMatch() {
        // Arrange
        let testValue = "2007"
        let dateComponents = DateComponents(year: Int(testValue), month: 1, day: 1)
        let expectedResult = Calendar.current.date(from: dateComponents)

        // Act
        let result = Date.start(of: testValue)

        // Assert
        XCTAssertEqual(expectedResult, result)
    }

    // MARK: -
    // MARK: end:of TESTS

    func testEndOf_WithYearString_DateShouldMatch() {
        // Arrange
        let testValue = "2007"
        let dateComponents = DateComponents(year: Int(testValue), month: 12, day: 31)
        let expectedResult = Calendar.current.date(from: dateComponents)

        // Act
        let result = Date.end(of: testValue)

        // Assert
        XCTAssertEqual(expectedResult, result)
    }
    
    // MARK: -
    // MARK: toISOFormattedString TESTS

    func testToISOFormattedString_WithYearString_DateShouldMatch() {
        // Arrange
        let dateComponents = DateComponents(year: 2020, month: 12, day: 31)
        let date = Calendar.current.date(from: dateComponents)

        // Act
        let result = date?.toISOFormattedString()

        // Assert
        XCTAssertEqual("2020-12-31T00:00:00.000+0000", result)
    }
    
}
