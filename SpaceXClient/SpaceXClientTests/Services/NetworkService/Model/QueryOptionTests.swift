//
//  QueryOptionTests.swift
//  SpaceXClientTests
//
//  Created by Colin Laffan on 14/02/2022.
//

import XCTest
@testable import SpaceXClient

class QueryOptionTests: XCTestCase {

    // MARK: -
    // MARK: LaunchYearDates TESTS
    
    func testLaunchYearDatesInit_WithInvalidInjectedYear_ShouldReturnNil() {
        // Arrange
        let testValue = "INVALID"

        // Act
        let result = LaunchYearDates(with: testValue)

        // Assert
        XCTAssertNil(result)
    }
    
    func testLaunchYearDatesInit_WithValidInjectedYear_ShouldNotReturnNil() {
        // Arrange
        let testValue = "2020"

        // Act
        let result = LaunchYearDates(with: testValue)

        // Assert
        XCTAssertNotNil(result)
    }
    
    // MARK: -
    // MARK: LaunchYearQueryOption TESTS

    func testLaunchYearQueryOptionInit_WithNoInjectedYears_ShouldReturnNil() {
        // Arrange
        let testValue = [String]()

        // Act
        let result = LaunchYearQueryOption(with: testValue)

        // Assert
        XCTAssertNil(result)
    }

    func testLaunchYearQueryOptionInit_WithInjectedYears_ShouldNotReturnNil() {
        // Arrange
        let testValue = ["2007"]

        // Act
        let result = LaunchYearQueryOption(with: testValue)

        // Assert
        XCTAssertNotNil(result)
    }
    
}
