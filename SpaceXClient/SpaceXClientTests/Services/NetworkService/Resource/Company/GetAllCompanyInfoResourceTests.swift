//
//  GetAllCompanyInfoResourceTests.swift
//  SpaceXClientTests
//
//  Created by Colin Laffan on 14/02/2022.
//

import XCTest
@testable import SpaceXClient

class GetAllCompanyInfoResourceTests: XCTestCase {

    // MARK: -
    // MARK: GetAllCompanyInfoResource TESTS
    
    func testGetAllCompanyInfoResource_Init_ShouldReturnMatchingValues() {
        // Arrange

        // Act
        let result = GetAllCompanyInfoResource()

        // Assert
        XCTAssertEqual("company", result.path)
        XCTAssertEqual(.GET, result.httpMethod)
        XCTAssertNil(result.httpBody)
        XCTAssertNil(result.headers)
    }

}
