//
//  PostAllLaunchesResourceTests.swift
//  SpaceXClientTests
//
//  Created by Colin Laffan on 14/02/2022.
//

import XCTest
@testable import SpaceXClient

class PostAllLaunchesResourceTests: XCTestCase {

    // MARK: -
    // MARK: GetAllCompanyInfoResource TESTS
    
    func testPostAllLaunchesResource_Init_ShouldReturnMatchingValues() {
        // Arrange
        let sortOrder = NetworkServiceConstantsEnums.SortOrder.Ascending
        let queryOptions: [QueryOption]? = nil

        // Act
        let result = PostAllLaunchesResource(with: sortOrder, and: queryOptions)

        // Assert
        XCTAssertEqual("launches/query", result.path)
        XCTAssertEqual(.POST, result.httpMethod)
        XCTAssertNotNil(result.httpBody)
        XCTAssertNotNil(result.headers)
    }

    // MARK: -
    // MARK: Mapper TESTS
    
    func testMapSortOrderToOptionsDTO_WithSortOrder_ShouldReturnOptionsDTO() {
        // Arrange
        let sortOrder = NetworkServiceConstantsEnums.SortOrder.Ascending
        let sut = PostAllLaunchesResource(with: sortOrder, and: nil)

        // Act
        let result = sut.mapSortOrderToOptionsDTO(sortOrder)

        // Assert
        XCTAssertFalse(result.pagination)
        XCTAssertEqual(1, result.populate.count)
        XCTAssertEqual("rocket", result.populate.first)
        XCTAssertEqual(sortOrder.rawValue, result.sort.dateUTC)
    }
    
    func testMapSuccessQueryOptionToSuccessDTO_WithNoSuccessfulLaunchQueryOption_ShouldReturnNil() {
        // Arrange
        let successQueryOption: SuccessfulLaunchQueryOption? = nil
        let sut = PostAllLaunchesResource(with: .Ascending, and: nil)

        // Act
        let result = sut.mapSuccessQueryOptionToSuccessDTO(successQueryOption)

        // Assert
        XCTAssertNil(result)
    }
    
    func testMapSuccessQueryOptionToSuccessDTO_WithSuccessfulLaunchQueryOption_ShouldReturnDTO() {
        // Arrange
        let successQueryOption = SuccessfulLaunchQueryOption(with: [.True])
        let sut = PostAllLaunchesResource(with: .Ascending, and: nil)

        // Act
        let result = sut.mapSuccessQueryOptionToSuccessDTO(successQueryOption)

        // Assert
        XCTAssertNotNil(result)
        XCTAssertEqual(1, result?.successIn.count)
        XCTAssertEqual(true, result?.successIn.first)
    }
    
}
