//
//  JSONLoader.swift
//  SpaceXClientTests
//
//  Created by Colin Laffan on 14/02/2022.
//

import Foundation
@testable import SpaceXClient

enum JsonFileNames: String {
    case CompanyInfoDTOData
}

class JSONLoader {

    func load(fileName: JsonFileNames) -> Data? {
        let bundle = Bundle(for: type(of: self))

        guard let url = bundle.url(forResource: fileName.rawValue, withExtension: "json") else {
            return nil
        }

        guard let data: Data = NSData(contentsOf: url) as Data? else {
            return nil
        }

        return data
    }

}
